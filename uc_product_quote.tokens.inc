<?php

/**
 * @file
 * Token hooks for the uc_product_quote module.
 */

/**
 * Implements hook_token_info().
 */
function uc_product_quote_token_info() {



  $types = array();
  $types['uc_product_quote'] = array(
    'name' => t('Quote'),
    'description' => t('Tokens for Ubercart product quotes.'),
    'needs-data' => 'quote',
  );


  $tokens = array();


  $tokens['uc_product_quote']['customer-requested-quotes'] = array(
    'name' => t('Customer - requested quotes'),
    'description' => t('A link to the overview of the requested quotes for a customer.'),
  );
  $tokens['uc_product_quote']['customer-published-quotes'] = array(
    'name' => t('Customer - published quotes'),
    'description' => t('A link to the overview of the published quotes for a customer.'),
  );
  $tokens['uc_product_quote']['customer-quote'] = array(
    'name' => t('Customer - quote'),
    'description' => t('A link to a quote for a customer.'),
  );

  $tokens['uc_product_quote']['shopadmin-requested-quotes'] = array(
    'name' => t('Shop administrator - requested quotes'),
    'description' => t('A link to the overview of the requested quotes for a shop administrator.'),
  );
  $tokens['uc_product_quote']['shopadmin-published-quotes'] = array(
    'name' => t('Shop administrator - published quotes'),
    'description' => t('A link to the overview of the published quotes for a shop administrator.'),
  );
  $tokens['uc_product_quote']['shopadmin-quote'] = array(
    'name' => t('Shop administrator - publish a quote'),
    'description' => t('A link to edit and publish a quote for a shop administrator.'),
  );


  return array(
    'types' => $types,
    'tokens' => $tokens,
  );



}

/**
 * Implements hook_tokens().
 */
function uc_product_quote_tokens($type, $tokens, $data = array(), $options = array()) {
  global $language;
  $language_code = NULL;
  if (isset($options['language'])) {
    $language = $options['language'];
    $language_code = $options['language']->language;
  }
  $t_options = array(
    'langcode' => $language_code,
    'context' => NULL,
  );
  $l_options = array(
    'language' => $language,
  );
  $replacements = array();
  $sanitize = !empty($options['sanitize']);



  if ($type == 'uc_product_quote' && !empty($data['quote'])) {

    $quote = $data['quote'];
    $customer_quote_requests = 'user/' . $quote->field_customer_uid[$quote->language][0]['value'] . '/quotesrequested/';
    $customer_quotes = 'user/' . $quote->field_customer_uid[$quote->language][0]['value'] . '/quotes/';
    $shopadmin_quote_requests = 'quotemanagement/quotesrequested/';
    $shopadmin_quotes = 'quotemanagement/quotespublished/';
    $quote_title = $quote->title;
    $customer_quote_path = 'node/' . $quote->nid;
    $shopadmin_quote_path = 'Productquote/' . $quote->nid . '/edit';

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'customer-requested-quotes':
          $url = url($customer_quote_requests, array('absolute' => TRUE, 'language' => $language));
          $replacements[$original] = l(t('My requests for a quote', array(),$t_options ), $url, $l_options );
          break;
        case 'customer-published-quotes':
          $url = url($customer_quotes, array('absolute' => TRUE, 'language' => $language));
          $replacements[$original] = l(t('My quotes', array(),$t_options ), $url, $l_options );
          break;
        case 'customer-quote':
          $url = url($customer_quote_path, array('absolute' => TRUE, 'language' => $language));
          $replacements[$original] = l(t('@quote_title ', array('@quote_title' => $quote_title),$t_options ), $url, $l_options );
          break;
        case 'shopadmin-requested-quotes':
          $url = url($shopadmin_quote_requests, array('absolute' => TRUE, 'language' => $language));
          $replacements[$original] = l(t('Overview : requests for a quote', array(),$t_options ), $url, $l_options );
          break;
        case 'shopadmin-published-quotes':
          $url = url($shopadmin_quotes, array('absolute' => TRUE, 'language' => $language));
          $replacements[$original] = l(t('Overview : published quotes', array(),$t_options ), $url, $l_options );
          break;
        case 'shopadmin-quote':
          $url = url($shopadmin_quote_path, array('absolute' => TRUE, 'language' => $language));
          $replacements[$original] = l(t('Edit and publish Quote @quote_title', array('@quote_title' => $quote_title),$t_options ), $url, $l_options );
          break;
      }
    }
  }


  return $replacements;
}


