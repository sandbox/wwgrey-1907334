<?php

/**
 * @file
 * Variable module hook implementations.
 */


/**
 * Implements hook_variable_group_info().
 */
function uc_product_quote_variable_group_info() {
  $groups['uc_product_quote'] = array(
    'title' => t('Ubercart product quote settings'),
    'access' => 'administer store',
    'path' => array('admin/store/settings/products'),
  );
  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function uc_product_quote_variable_info($options) {
  $variables['uc_product_quote_add_to_cart_text_catalog_view'] = array(
    'type' => 'text',
    'title' => t("'Add to Cart' Button Text for catalog view", array(), $options),
    'description' => t('Adjust the text of the submit button for <em>Add to Cart</em> on the catalog view.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_add_to_cart_text_details_view'] = array(
    'type' => 'text',
    'title' => t("'Add to Cart' Button Text for product details view", array(), $options),
    'description' => t('Adjust the text of the submit button for <em>Add to Cart</em> on the product details form.', array(), $options),
    'group' => 'uc_product_quote',
    'default' => 'Request a Quote',
  );
  $variables['uc_product_quote_product_details_price_text'] = array(
    'type' => 'text',
    'title' => t('Product details: price Text', array(), $options),
    'description' => t('Adjust the text of the <em>Sell Price</em> on the product details pages.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_instruction_title'] = array(
    'type' => 'text',
    'title' => t('Quote request: instruction title', array(), $options),
    'description' => t('Adjust the title on the request a quote form.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_instruction_header'] = array(
    'type' => 'text',
    'title' => t('Quote request: instruction header', array(), $options),
    'description' => t('Adjust the text of the instruction header on the quote request form.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_instruction_footer'] = array(
    'type' => 'text',
    'title' => t('Quote request: instruction footer', array(), $options),
    'description' => t('Adjust the text of the instruction footer on the quote request form.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_instruction_email'] = array(
    'type' => 'text',
    'title' => t('Quote request: instruction email address', array(), $options),
    'description' => t('Adjust the text of the instruction to define a correct e-mail address.', array(), $options),
    'group' => 'uc_product_quote',
  );

  $variables['uc_product_quote_request_a_quote_submit'] = array(
    'type' => 'text',
    'title' => t('Quote request: general confirmation message', array(), $options),
    'description' => t('General message displayed when a user has requested a quote.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_request_a_quote_logged_in'] = array(
    'type' => 'text',
    'title' => t('Quote request: confirmation message when user is logged in', array(), $options),
    'description' => t('Adjust the text for a quote request confirmation message when user is logged in.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_request_a_quote_existing_user'] = array(
    'type' => 'text',
    'title' => t('Quote request: confirmation message when existing user', array(), $options),
    'description' => t('Adjust the text for a quote request  confirmation message when user exists.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_request_a_quote_new_user'] = array(
    'type' => 'text',
    'title' => t('Quote request: confirmation message when new user created', array(), $options),
    'description' => t('Adjust the text for a quote request confirmation message when new user is created.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_request_a_quote_new_user_logged_in'] = array(
    'type' => 'text',
    'title' => t('Quote request: confirmation message when new user created and logged in', array(), $options),
    'description' => t('Adjust the text for a quote request confirmation message when new user is created and logged in.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_request_a_quote_continue_shopping'] = array(
    'type' => 'text',
    'title' => t('Quote request: continue shopping message', array(), $options),
    'description' => t('Adjust the text to tell the user how to continue shopping when the quote request is created.', array(), $options),
    'group' => 'uc_product_quote',
  );

  $variables['uc_product_quote_edit_quote_instruction_header'] = array(
    'type' => 'text',
    'title' => t('Publish a quote: instruction header', array(), $options),
    'description' => t('Adjust the text of the instruction header on the publish quote form.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_edit_quote_instruction_footer'] = array(
    'type' => 'text',
    'title' => t('Publish a quote: instruction footer', array(), $options),
    'description' => t('Adjust the text of the instruction footer on the on the publish quote form.', array(), $options),
    'group' => 'uc_product_quote',
  );

  $variables['uc_product_quote_sell_price_description'] = array(
    'type' => 'text',
    'title' => t('Sell price: description', array(), $options),
    'description' => t('Adjust the description for the sell price field on the quote publishing form.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_list_price_description'] = array(
    'type' => 'text',
    'title' => t('List price: description', array(), $options),
    'description' => t('Adjust the description for the list price field on the quote publishing form.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_cost_description'] = array(
    'type' => 'text',
    'title' => t('Cost price: description', array(), $options),
    'description' => t('Adjust the description for the cost price field on the quote publishing form.', array(), $options),
    'group' => 'uc_product_quote',
  );




  $variables['uc_product_quote_request_a_quote_email_confirmation_introduction'] = array(
    'type' => 'text',
    'title' => t('Request a quote - email confirmation - Introduction text', array(), $options),
    'description' => t('Introduction text for the e-mail that confirms the request for a quote by a customer.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_request_a_quote_email_confirmation_help'] = array(
    'type' => 'text',
    'title' => t('Request a quote - email confirmation - Help text', array(), $options),
    'description' => t('Help text for the e-mail that confirms the request for a quote by a customer.', array(), $options),
    'group' => 'uc_product_quote',
  );

  $variables['uc_product_quote_request_a_quote_email_notification_introduction'] = array(
    'type' => 'text',
    'title' => t('Request a quote - email notification - Introduction text', array(), $options),
    'description' => t('Introduction text for the e-mail that notifies the shop administrator of a new request for a quote .', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_request_a_quote_email_notification_help'] = array(
    'type' => 'text',
    'title' => t('Request a quote - email notification - Help text', array(), $options),
    'description' => t('Help text for the e-mail that notifies the shop administrator of a new request for a quote .', array(), $options),
    'group' => 'uc_product_quote',
  );

  $variables['uc_product_quote_published_quote_email_confirmation_introduction'] = array(
    'type' => 'text',
    'title' => t('Quote publishing - email confirmation - Introduction text', array(), $options),
    'description' => t('Introduction text for the e-mail that confirms the publishing of a quote to the shop administrator.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_published_quote_email_confirmation_help'] = array(
    'type' => 'text',
    'title' => t('Quote publishing - email confirmation - Help text', array(), $options),
    'description' => t('Help text for for the e-mail that confirms the publishing of a quote to the shop administrator..', array(), $options),
    'group' => 'uc_product_quote',
  );

  $variables['uc_product_quote_published_quote_email_notification_introduction'] = array(
    'type' => 'text',
    'title' => t('Quote publishing - email notification - Introduction text', array(), $options),
    'description' => t('Introduction text for the e-mail that notifies the customer of the publishing of a quote.', array(), $options),
    'group' => 'uc_product_quote',
  );
  $variables['uc_product_quote_published_quote_email_notification_help'] = array(
    'type' => 'text',
    'title' => t('Quote publishing - email notification - Help text', array(), $options),
    'description' => t('Help text for the e-mail that notifies the customer of the publishing of a quote.', array(), $options),
    'group' => 'uc_product_quote',
  );


  return $variables;
}
