<?php
/**
 * @file
 *   - definition of some fixed variables
 *
 */

// request completion message identifiers
define('USER_LOGGED_IN', 'uc_product_quote_request_a_quote_logged_in');
define('USER_EXISTS', 'uc_product_quote_request_a_quote_existing_user');
define('USER_CREATED', 'uc_product_quote_request_a_quote_new_user');
define('USER_CREATED_WITH_LOGIN', 'uc_product_quote_request_a_quote_new_user_logged_in');

// quote status codes
define('QUOTE_REQUESTED', 0);
define('QUOTE_PUBLISHED', 1);
define('QUOTE_ORDERED', 2);
define('QUOTE_CANCELED', 3);
define('QUOTE_EXPIRED', 4);