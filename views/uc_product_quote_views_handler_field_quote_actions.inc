<?php

/**
 * @file
 * Views handler: Return actions for a quote
 */

/**
 * Return actions for a quote.
 */
class uc_product_quote_views_handler_field_quote_actions extends views_handler_field {



  /**
   *
   */
  function render($values) {
    global $user;
    $quote = node_load($values->nid);
    $actions = array();

    // actions are based on the status of the quote
    if (isset($quote->field_quote_status[$quote->language][0]['value'])) {
      if (isset($quote->field_customer_uid[$quote->language][0]['value'])) {$customer_uid = $quote->field_customer_uid[$quote->language][0]['value'];}
      if (isset($quote->field_provider_uid[$quote->language][0]['value'])) {$provider_uid = $quote->field_provider_uid[$quote->language][0]['value'];}
      if (isset($quote->field_provider_rid[$quote->language][0]['value'])) {$provider_rid = $quote->field_provider_rid[$quote->language][0]['value'];}
      $quote_status = $quote->field_quote_status[$quote->language][0]['value'];
      switch  ($quote_status) {
        case QUOTE_REQUESTED:
          if ($user->uid == $customer_uid || $user->uid == $provider_uid ||  (in_array('administrator', array_values($user->roles))) ) {
            $alt = t('View quote @quote_name.', array('@quote_name' => $quote->title,));
            $actions[] = array(
              'name' => t('View'),
              'url' => 'node/' . $quote->nid ,
              'icon' => theme('image', array('path' => drupal_get_path('module', 'uc_product_quote') . '/images/quote_view.png', 'alt' => $alt)),
              'title' => $alt,
            );
          }
          if ($user->uid == $provider_uid ||  (in_array('administrator', array_values($user->roles))) ) {
            $alt = t('Edit quote @quote_name.', array('@quote_name' => $quote->title,));
            $actions[] = array(
              'name' => t('Edit'),
              'url' => 'productquote/' . $quote->nid . '/edit',
              'icon' => theme('image', array('path' => drupal_get_path('module', 'uc_product_quote') . '/images/quote_edit.png', 'alt' => $alt)),
              'title' => $alt,
            );
          }
          /* seems not very usefu to have a publish action without edit screen
          if ($user->uid == $provider_uid ||  (in_array('administrator', array_values($user->roles))) ) {
            $alt = t('Publish quote @quote_name.', array('@quote_name' => $quote->title,));
            $actions[] = array(
              'name' => t('Publish'),
              'url' => 'Productquote/' . $quote->nid . '/publish',
              'icon' => theme('image', array('path' => drupal_get_path('module', 'uc_product_quote') . '/images/quote_publish.gif', 'alt' => $alt)),
              'title' => $alt,
            );
          }
          */
          if ((in_array('administrator', array_values($user->roles))) ) {
            $alt = t('Delete quote @quote_name.', array('@quote_name' => $quote->title,));
            $actions[] = array(
              'name' => t('Delete'),
              'url' => 'productquote/' . $quote->nid . '/delete',
              'icon' => theme('image', array('path' => drupal_get_path('module', 'uc_product_quote') . '/images/quote_delete.gif', 'alt' => $alt)),
              'title' => $alt,
            );
          }
          break;

        case QUOTE_PUBLISHED:
          if ($user->uid == $customer_uid || $user->uid == $provider_uid ||  (in_array('administrator', array_values($user->roles))) ) {
            $alt = t('View quote request @quote_name.', array('@quote_name' => $quote->title,));
            $actions[] = array(
              'name' => t('View'),
              'url' => 'node/' . $quote->nid ,
              'icon' => theme('image', array('path' => drupal_get_path('module', 'uc_product_quote') . '/images/quote_view.png', 'alt' => $alt)),
              'title' => $alt,
            );
          }
          if ($user->uid == $provider_uid ||  (in_array('administrator', array_values($user->roles))) ) {
            $alt = t('Unpublish quote @quote_name.', array('@quote_name' => $quote->title,));
            $actions[] = array(
              'name' => t('Unpublish'),
              'url' => 'productquote/' . $quote->nid . '/unpublish',
              'icon' => theme('image', array('path' => drupal_get_path('module', 'uc_product_quote') . '/images/quote_unpublish.gif', 'alt' => $alt)),
              'title' => $alt,
            ) ;
          }

          break;
        case QUOTE_ORDERED:
          break;
        case QUOTE_CANCELED:
          break;
        case QUOTE_EXPIRED:
          break;

      }

    }

    $output = '';
    foreach ($actions as $action) {
      $output .= l($action['icon'], $action['url'], array('attributes' => array('title' => $action['title']), 'html' => TRUE));
    }
    return $output;
  }

  /**
   *
   */
  function query() {
    // Do nothing.
  }

}
