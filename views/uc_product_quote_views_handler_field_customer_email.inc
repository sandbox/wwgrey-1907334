<?php

/**
 * @file
 * Views handler: Returns email of the customer
 *
 *
 */

/**
 * Return the order sell price , in the currency of the customer.
 */
class uc_product_quote_views_handler_field_customer_email extends views_handler_field {

  /**
   *
   */
  function render($values) {

    $nid = $values->nid;
    $quote = node_load($nid);
    $customer_uid = $quote->field_customer_uid['und'][0]['value'];
    $customer = user_load($customer_uid);
    $customer_email = check_markup($customer->mail, 'plain_text');
    $output = $customer_email;
    return $output;
  }

  /**
   *
   */
  function query() {
    // Do nothing.
  }

}
