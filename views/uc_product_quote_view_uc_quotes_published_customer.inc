<?php

$view = new view();
$view->name = 'uc_quotes_published_customer';
$view->description = '';
$view->tag = 'Ubercart';
$view->base_table = 'node';
$view->human_name = 'uc_quotes_published_customer';
$view->core = 0;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['title'] = 'My quotes';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  2 => '2',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'actions_1' => 'actions_1',
  'title' => 'title',
  'model' => 'model',
  'changed' => 'changed',
  'created' => 'created',
);
$handler->display->display_options['style_options']['default'] = 'changed';
$handler->display->display_options['style_options']['info'] = array(
  'actions_1' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'model' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
/* Relationship: Content revision: User */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node_revision';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
$handler->display->display_options['relationships']['uid']['label'] = 'Last update by  user';
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
/* Field: Product: Quote Actions */
$handler->display->display_options['fields']['quote_actions']['id'] = 'quote_actions';
$handler->display->display_options['fields']['quote_actions']['table'] = 'uc_products';
$handler->display->display_options['fields']['quote_actions']['field'] = 'quote_actions';
$handler->display->display_options['fields']['quote_actions']['label'] = 'Actions';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
/* Field: Product: SKU */
$handler->display->display_options['fields']['model']['id'] = 'model';
$handler->display->display_options['fields']['model']['table'] = 'uc_products';
$handler->display->display_options['fields']['model']['field'] = 'model';
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Updated';
$handler->display->display_options['fields']['changed']['hide_alter_empty'] = FALSE;
$handler->display->display_options['fields']['changed']['date_format'] = 'uc_store';
/* Field: Content: Post date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'uc_store';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
$handler->display->display_options['sorts']['created']['expose']['label'] = 'Post date';
$handler->display->display_options['sorts']['created']['granularity'] = 'day';
/* Sort criterion: Content: Updated date */
$handler->display->display_options['sorts']['changed']['id'] = 'changed';
$handler->display->display_options['sorts']['changed']['table'] = 'node';
$handler->display->display_options['sorts']['changed']['field'] = 'changed';
$handler->display->display_options['sorts']['changed']['expose']['label'] = 'Updated date';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
$handler->display->display_options['sorts']['title']['expose']['label'] = 'Title';
/* Contextual filter: Content: Customer uid (field_customer_uid) */
$handler->display->display_options['arguments']['field_customer_uid_value']['id'] = 'field_customer_uid_value';
$handler->display->display_options['arguments']['field_customer_uid_value']['table'] = 'field_data_field_customer_uid';
$handler->display->display_options['arguments']['field_customer_uid_value']['field'] = 'field_customer_uid_value';
$handler->display->display_options['arguments']['field_customer_uid_value']['default_action'] = 'not found';
$handler->display->display_options['arguments']['field_customer_uid_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_customer_uid_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_customer_uid_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_customer_uid_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_customer_uid_value']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['field_customer_uid_value']['validate']['type'] = 'php';
$handler->display->display_options['arguments']['field_customer_uid_value']['validate_options']['code'] = '// check if it is the user himself using the view
global $user;
$result = $user->uid == $argument ? TRUE : FALSE;
return $result;';
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'quote' => 'quote',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
$handler->display->display_options['filters']['status']['group'] = 1;
/* Filter criterion: Content: Quote status (field_quote_status) */
$handler->display->display_options['filters']['field_quote_status_value']['id'] = 'field_quote_status_value';
$handler->display->display_options['filters']['field_quote_status_value']['table'] = 'field_data_field_quote_status';
$handler->display->display_options['filters']['field_quote_status_value']['field'] = 'field_quote_status_value';
$handler->display->display_options['filters']['field_quote_status_value']['value']['value'] = '1';
$handler->display->display_options['filters']['field_quote_status_value']['group'] = 1;

/* Display: list */
$handler = $view->new_display('page', 'list', 'admin_page');
$handler->display->display_options['display_description'] = 'List of published quotes';
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['path'] = 'user/%/quotes';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Published quotes';
$handler->display->display_options['menu']['description'] = 'List of published quotes';
$handler->display->display_options['menu']['weight'] = '5';
$handler->display->display_options['menu']['name'] = 'user-menu';
$handler->display->display_options['menu']['context'] = 0;
$translatables['uc_quotes_published_customer'] = array(
  t('Defaults'),
  t('My quotes'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Last update by  user'),
  t('Nid'),
  t('Actions'),
  t('Title'),
  t('SKU'),
  t('Updated'),
  t('Post date'),
  t('Updated date'),
  t('All'),
  t('list'),
  t('List of published quotes'),
);


$views[$view->name] = $view;
