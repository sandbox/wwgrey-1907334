<?php

/**
 * @file
 * Views hooks and callback registries.
 */

/**
 * Implements hook_views_data().
 */



/**
 * implement hook_views_data_alter
 */

function uc_product_quote_views_data_alter(&$data) {

  // add a field for the product record that will be used as an 'actions' column on the view
  $data['uc_products'] ['quote_actions'] = array(
    'title' => t('Quote Actions'),
    'help' => t('Clickable links to actions a user may perform on a Quote.'),
    'field' => array(
      'handler' => 'uc_product_quote_views_handler_field_quote_actions',
      'group' => 'Product',
      'click sortable' => FALSE,
    ),
  );


  $data['uc_products']['customer_email'] = array(
    'title' => t('Customer e-mail'),
    'help' => t('Email of the customer that has requested the quote.'),
    'field' => array(
      'handler' => 'uc_product_quote_views_handler_field_customer_email',
      'group' => 'Product',
      'click sortable' => TRUE,
    ),
  );


}