<?php

/**
 * @file
 * Hooks provided by the uc_product_quote module.
 */

/**
 * @addtogroup hooks
 *
 */


/**
 * HOOK_uc_product_quote_quote_details_alter: alters quote details like the SKU number when a quote is created
 *
 *
 * @param $quote details
 *   An associative array with quote parameters that may be modified
 *     'sku' => the default SKU for a quote is 'quote-requestnodeid-requestquotecounter';
 *     'customer_uid' => userid that may view access rights on the quote
 *     'provider_uid' => userid that that that will be granted edit access on quote
 *           by default , this is the administrator (uid = 1)
 *     'provider_rid' =>  role id of role that will be granted edit access on quote
 *           by default , this is an empty array
 *     'tid' => taxonomy term of the product catalog, used to publish the quote
 *           by default set in uc_product_quote setting, but can be dynamically overwritten here
 *
 *
 * @param $request_a_quote
 *   this is a node of a given ubercart product class (by default 'request_a_quote').
 *   It is the product for which the request a quote is raised
 *
 */
function HOOK_uc_product_quote_details_alter(&$quote_details, $request_a_quote) {

  if (isset($request_a_quote->shop)) {
    $shop = $request_a_quote->shop;
    $quote_details['provider_uid'] = $shop->shop_uid;

    $sku = substr($shop->shop_name, 0, 5);
    $sku .= '-' . $shop->shop_id . '-' . $quote_details['sku'];
    $quote_details['sku'] = $sku;
  }


}

/**
 * HOOK_uc_product_quote_get_address_list
 *
 *  get a list of addresses given a user account and a request for a quote
 *
 * @param $account
 *    user object
 * @param   $request_a_quote
 *    the request a quote object for which we need address list
 *
 * @return $adress_list
 *    an indexed  array with addresses
 *    address_list = array (
 *       0 => address_1,
 *       1 => address_2,
 *     )
 *   Each address is an associative array with following structure
 *
 *   $address_list[] = array (
 *       'first_name' => 'Jean',
 *       'last_name' => 'Vermeulen',
 *       'company' => 'companyname',
 *       'street1' => 'street name',
 *       'street2' => 'street name part 2',
 *       'city' => 'city',
 *       'zone' => '',
 *       'postal_code' => 'postal code zip code',
 *       'country' => '',
 *       'phone' => '+44 22 33 66 88',
 *       'default' => FALSE,
 *       );
 *
 *    remarks :
 *     'default ' set on TRUE for the address selected by default in the select list
 *     'zone' and 'country' : specify the zone and country ids as defined in the uc_countries and uc_zones ubercart tables
 *
 *
 *
 */
function HOOK_uc_product_quote_get_address_list($account, $request_a_quote) {



  $address_list = array();
  field_attach_load('user', array($account->uid => $account), FIELD_LOAD_CURRENT,  array('field_id' => 'field_address') );

  if (isset($account->field_address['und'][0])) {
    $address = $account->field_address['und'][0];
    foreach ($address as $address_field_name => $address_field_value) {
      $address_list[0][$address_field_name] = $address_field_value;
    }
    // address kept on the user profile is the default one
    $address_list[0]['default'] = TRUE;
  }


  return $address_list;

}


/**
 * HOOK_uc_product_quote_save_address
 *
 *  save the address in tha address book
 *  when a customer requests for a quote, some contact info can be requested
 *  wit this hook , the contact info can be saved in an address book
 *
 * @param $uid
 *    userid of the user to which this address belongs
 * @param   $user_status
 *    status of the user when this request to save the address is made
 *      include uc_product_quote_definitions.inc to get following constants
 *       USER_CREATED: user not logged in, user account has been created
 *       USER_CREATED_WITH_LOGIN: user account has been created and aut logged in user
 *       USER_EXISTS: user not logged in, but user account exists (based on email address)
 *       USER_LOGGED_IN: user is logged in, existing account
 * @param   $quote
 *    the quote
 *
 * @param   $address
 *   The address
 *   Each address is an associative array with following structure
 *
 *   $address = array (
 *       'first_name' => 'Jean',
 *       'last_name' => 'Vermeulen',
 *       'company' => 'companyname',
 *       'street1' => 'street name',
 *       'street2' => 'street name part 2',
 *       'city' => 'city',
 *       'zone' => '',
 *       'postal_code' => 'postal code zip code',
 *       'country' => '',
 *       'phone' => '+44 22 33 66 88',
 *       'default' => FALSE,
 *       );
 *
 *    remarks :
 *     'default ' set on TRUE for the address selected by default in the select list
 *     'zone' and 'country' : specify the zone and country ids as defined in the uc_countries and uc_zones ubercart tables
 *
 * @return   TRUE/FALSE
 *     TRUE if the address has been saved
 *
 */
function HOOK_uc_product_quote_save_address($uid, $user_status, $quote, $address) {


  // get definitions
  require_once(drupal_get_path('module', 'uc_product_quote') . '/includes/uc_product_quote_definitions.inc');
  $update_flag = FALSE;

  // don't update address book for existing user, with data from anonymous user
  // don't update address book for anonymous user
  if ($user_status == USER_EXISTS || $uid == 0) {
    return $update_flag;
  }

  $account = user_load($uid);
  foreach ($address as $field_name => $field_value) {
    if (!is_null($field_value) && $field_value != '') {
      if (!isset($account->field_address['und'][0][$field_name])) {
        $update_flag = TRUE;
        $account->field_address['und'][0][$field_name] = $field_value;
      }
      elseif ($account->field_address['und'][0][$field_name] != $field_value) {
        $update_flag = TRUE;
        $account->field_address['und'][0][$field_name] = $field_value;
      }
    }
  }
  if ($update_flag) {
    field_attach_update('user', $account);
  }

  return $update_flag;



}
