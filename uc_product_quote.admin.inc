<?php

/**
 * @file
 * Product Quote administration menu items.
 */

/**
 * Form builder for product quote settings
 */
function uc_product_quote_settings() {



  // check ubercart settings : do we allow anonymous checkout ?
  $anon_state = array('visible' => array('input[name="uc_checkout_anonymous"]' => array('checked' => TRUE)));

  $form['uc_product_quote_settings'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => t('general'),
  );



  /*
      general product quote settings
  */
  $form['general'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('General'),
  );

  $form['general']['uc_product_quote_expiration_days'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 4,
    '#title' => t('Expiration date: number of days the quote will be valid'),
    '#description' => t('Specify the number of days the quote will be valid starting from the publishing date.'),
    '#default_value' => variable_get('uc_product_quote_expiration_days', 30),
  );
  $form['general']['uc_product_quote_expiration_days_editable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Editable expiration date'),
    '#description' => t('Specify if the expiration date is editable.'),
    '#default_value' => variable_get('uc_product_quote_expiration_days_editable', TRUE),
  );

  $form['general']['uc_product_quote_sell_price_editable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Editable sell price'),
    '#description' => t('Specify if the sell price is editable on the quote publishing form.'),
    '#default_value' => variable_get('uc_product_quote_sell_price_editable', TRUE),
  );
  $form['general']['uc_product_quote_sell_price_visible'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sell price visible'),
    '#description' => t('Specify if the sell price is visible on the quote publishing form.'),
    '#default_value' => variable_get('uc_product_quote_sell_price_visible', TRUE),
  );
  $form['general']['uc_product_quote_sell_price_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Sell price: description'),
    '#description' => t('Adjust the description for the sell price field on the quote publishing form.'),
    '#default_value' => variable_get('uc_product_quote_sell_price_description', 'Specify the sell price for the quote'),
  );
  $form['general']['uc_product_quote_sell_price_maximum'] = array(
    '#type' => 'textfield',
    '#title' => t('Sell price: maximum value'),
    '#description' => t('To prevent errors, specify here the maximum sell price allowed for a quote.'),
    '#default_value' => variable_get('uc_product_quote_sell_price_maximum', 100000),
  );
  $form['general']['uc_product_quote_sell_price_minimum'] = array(
    '#type' => 'textfield',
    '#title' => t('Sell price: minimum value'),
    '#description' => t('To prevent errors, specify here the minimum sell price allowed for a quote.'),
    '#default_value' => variable_get('uc_product_quote_sell_price_minimum', 0),
  );


  $form['general']['uc_product_quote_list_price_editable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Editable list price'),
    '#description' => t('Specify if the list price is editable on the quote publishing form.'),
    '#default_value' => variable_get('uc_product_quote_list_price_editable', TRUE),
  );
  $form['general']['uc_product_quote_list_price_visible'] = array(
    '#type' => 'checkbox',
    '#title' => t('List price visible'),
    '#description' => t('Specify if the list price is visible on the quote publishing form.'),
    '#default_value' => variable_get('uc_product_quote_list_price_visible', TRUE),
  );
  $form['general']['uc_product_quote_list_price_description'] = array(
    '#type' => 'textfield',
    '#title' => t('List price: description'),
    '#description' => t('Adjust the description for the list price field on the quote publishing form.'),
    '#default_value' => variable_get('uc_product_quote_list_price_description', 'Specify the list price for the quote'),
  );


  $form['general']['uc_product_quote_cost_editable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Editable cost price'),
    '#description' => t('Specify if the cost price is editable on the quote publishing form.'),
    '#default_value' => variable_get('uc_product_quote_cost_editable', TRUE),
  );
  $form['general']['uc_product_quote_cost_visible'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cost price visible'),
    '#description' => t('Specify if the sell price is visible on the quote publishing form.'),
    '#default_value' => variable_get('uc_product_quote_cost_visible', TRUE),
  );
  $form['general']['uc_product_quote_cost_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Cost price: description'),
    '#description' => t('Adjust the description for the cost price field on the quote publishing form.'),
    '#default_value' => variable_get('uc_product_quote_cost_description', 'Specify the cost price for the quote'),
  );

  /*
      address information
  */
  $form['address'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('Address specification'),
  );

  $form['address']['uc_product_quote_ubercart_address_list_billing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Ubercart billing address book'),
    '#description' => t('Use Ubercart order billing info to create a list of available addresses on the  request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_ubercart_address_list_billing', FALSE),
  );
  $form['address']['uc_product_quote_ubercart_address_list_delivery'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Ubercart delivery address book'),
    '#description' => t('Use Ubercart order delivery info to create a list of available addresses on the  request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_ubercart_address_list_delivery', FALSE),
  );

  $form['address']['uc_product_quote_first_name_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer first name enabled'),
    '#description' => t('Specify if the customer first name field must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_first_name_enabled', TRUE),
  );
  $form['address']['uc_product_quote_last_name_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer last name enabled'),
    '#description' => t('Specify if the customer last name field must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_last_name_enabled', TRUE),
  );

  $form['address']['uc_product_quote_company_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer company enabled'),
    '#description' => t('Specify if the customer company field must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_company_enabled', TRUE),
  );

  $form['address']['uc_product_quote_country_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer country enabled'),
    '#description' => t('Specify if the customer country field must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_country_enabled', TRUE),
  );

  $form['address']['uc_product_quote_zone_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer zone (State/region) enabled'),
    '#description' => t('Specify if the customer zone field (State/region) must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_zone_enabled', TRUE),
  );

  $form['address']['uc_product_quote_city_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer city enabled'),
    '#description' => t('Specify if the customer city field must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_city_enabled', TRUE),
  );

  $form['address']['uc_product_quote_postal_code_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer postal code enabled'),
    '#description' => t('Specify if the customer postal code field must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_postal_code_enabled', TRUE),
  );

  $form['address']['uc_product_quote_street1_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer street1 enabled'),
    '#description' => t('Specify if the customer street1 field must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_street1_enabled', TRUE),
  );

  $form['address']['uc_product_quote_street2_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer street2 enabled'),
    '#description' => t('Specify if the customer street2 field must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_street2_enabled', TRUE),
  );

  $form['address']['uc_product_quote_phone_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Customer phone enabled'),
    '#description' => t('Specify if the customer phone field must be enabled on the request for a quote form.'),
    '#default_value' => variable_get('uc_product_quote_phone_enabled', TRUE),
  );


  /*
      Catalog related settings
  */
  $form['catalog'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('Catalog settings'),
  );

  $form['catalog']['uc_product_quote_add_to_cart_text_catalog_view'] = array(
    '#type' => 'textfield',
    '#title' => t("'Add to Cart' Button Text for catalog view"),
    '#description' => t('Adjust the text of the submit button for <em>Add to Cart</em> on the catalog view.'),
    '#default_value' => variable_get('uc_product_quote_add_to_cart_text_catalog_view', 'Quote'),
  );
  $form['catalog']['uc_product_quote_add_to_cart_text_details_view'] = array(
    '#type' => 'textfield',
    '#title' => t("'Add to Cart' Button Text for product details view"),
    '#description' => t('Adjust the text of the submit button for <em>Add to Cart</em> on the product details form.'),
    '#default_value' => variable_get('uc_product_quote_add_to_cart_text_details_view','Request a Quote'),
  );
  // Get the vocabulary tree information.
  $vid = variable_get('uc_catalog_vid', 0);
  $tree = taxonomy_get_tree($vid);
  $options = array();
  foreach ($tree as $leaf) {
    $leaf_name = '';
    if ($leaf->depth == 1) {$leaf_name = '- ';}
    if ($leaf->depth == 2) {$leaf_name = '-- ';}
    if ($leaf->depth == 2) {$leaf_name = '--- ';}
    $leaf_name .= $leaf->name;
    $options[$leaf->tid] = $leaf_name;
  }
  $form['catalog']['uc_product_quote_quote_taxonomy_term'] = array(
    '#type' => 'select',
    '#title' => t('Catalog term for quotes'),
    '#options' => $options,
    '#description' => t('Set this to the taxonomy term from the ubercart catalog that will be used to assign the quotes to.'),
    '#default_value' => variable_get('uc_product_quote_quote_taxonomy_term',0),
  );
  $form['catalog']['uc_product_quote_product_details_price_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Product details: price Text'),
    '#size' => 120,
    '#description' => t('Adjust the text of the <em>Sell Price</em> on the product details pages.'),
    '#default_value' => variable_get('uc_product_quote_product_details_price_text',"This product cannot be ordered without a quote. Please, click on the 'Request a quote' button"),
  );

  /*
      'Request a Quote' instruction settings
  */


  $form['request_a_quote'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('Request a quote: instruction messages'),
  );

  $form['request_a_quote']['uc_product_quote_instruction_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Quote request: instruction title'),
    '#size' => 60,
    '#description' => t('Adjust the title on the quote request form. Leave blank if no title is required'),
    '#default_value' => variable_get('uc_product_quote_instruction_title', 'Request a Quote'),
  );

  $instruction_header = 'If you would like us to send you a quote, please fill in all fields below and provide us with enough information so we can provide an accurate reply.';
  $form['request_a_quote']['uc_product_quote_instruction_header'] = array(
    '#type' => 'textarea',
    '#title' => t('Quote request: instruction header'),
    '#description' => t('Adjust the text of the instruction header on the quote request form.'),
    '#default_value' => variable_get('uc_product_quote_instruction_header', $instruction_header),
  );
  $instruction_footer = "Please click on the 'SUBMIT' button so send your request to us. When our offer is available, we will send you a notification email.";
  $instruction_footer .= '</br>';
  $instruction_footer .= 'To view your current quote status and to order our offer once it is available, you will need to login.';
  $instruction_footer .= '</br>';
  $instruction_footer .= "After login, you can access the quote request and the offer under 'my account'.";
  $form['request_a_quote']['uc_product_quote_instruction_footer'] = array(
    '#type' => 'textarea',
    '#title' => t('Quote request: instruction footer'),
    '#description' => t('Adjust the text of the instruction footer on the quote request form.'),
    '#default_value' => variable_get('uc_product_quote_instruction_footer', $instruction_footer),
  );
  $instruction_email = 'Enter a valid email address or [site:login-link] to login with an existing account.';
  $instruction_email .= ' ' . 'Once the quote is available, a notification will be sent to this email address.';
  $instruction_email .= ' ' . 'Please make sure it is defined correctly.';
  $form['request_a_quote']['uc_product_quote_instruction_email'] = array(
    '#type' => 'textarea',
    '#title' => t('Quote request: instruction email address'),
    '#description' => t('Adjust the text of the instruction to define a correct e-mail address.'),
    '#default_value' => variable_get('uc_product_quote_instruction_email', $instruction_email),
  );

  if (module_exists('token')) {
    $form['request_a_quote']['token_tree'] = array(
      '#markup' => theme('token_tree', array('token_types' => array('node', 'site', 'store',  ))),
    );
  }


  /*
      'Request a Quote' completion messages
  */
  $form['request_a_quote_completion'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('Request a quote: completion messages'),
  );

  $form['request_a_quote_completion']['uc_product_quote_request_a_quote_submit'] = array(
    '#type' => 'textarea',
    '#title' => t('General message'),
    '#description' => t('General message displayed when a user has requested a quote.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_submit', uc_get_message('completion_request_a_quote_submit')),
    '#rows' => 3,
  );
  $form['request_a_quote_completion']['uc_product_quote_request_a_quote_logged_in'] = array(
    '#type' => 'textarea',
    '#title' => t('Logged in users'),
    '#description' => t('Message displayed after a user has requested a quote for a user who is logged in.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_logged_in', uc_get_message('completion_request_a_quote_logged_in')),
    '#rows' => 3,
  );
  $form['request_a_quote_completion']['uc_product_quote_request_a_quote_existing_user'] = array(
    '#type' => 'textarea',
    '#title' => t('Existing users'),
    '#description' => t("Message displayed after a user has requested a quote for a user who has an account but wasn't logged in."),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_existing_user', uc_get_message('completion_request_a_quote_existing_user')),
    '#rows' => 3,
    '#states' => $anon_state,
  );
  $form['request_a_quote_completion']['uc_product_quote_request_a_quote_new_user'] = array(
    '#type' => 'textarea',
    '#title' => t('New users'),
    '#description' => t("Message displayed after a user has requested a quote for a new user whose account was just created. You may use the special tokens !new_username for the username of a newly created account and !new_password for that account's password."),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_new_user', uc_get_message('completion_request_a_quote_new_user')),
    '#rows' => 3,
    '#states' => $anon_state,
  );
  $form['request_a_quote_completion']['uc_product_quote_request_a_quote_new_user_logged_in'] = array(
    '#type' => 'textarea',
    '#title' => t('New logged in users'),
    '#description' => t('Message displayed after a user has requested a quote for a new user whose account was just created and also <em>"Login users when new customer accounts are created."</em> is set on the <a href="!user_login_setting_ur">checkout settings</a>.', array('!user_login_setting_ur' => 'admin/store/settings/checkout')),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_new_user_logged_in', uc_get_message('completion_request_a_quote_new_user_logged_in')),
    '#rows' => 3,
    '#states' => $anon_state,
  );
  $form['request_a_quote_completion']['uc_product_quote_request_a_quote_continue_shopping'] = array(
    '#type' => 'textarea',
    '#title' => t('Continue shopping message'),
    '#description' => t('Message displayed after a user has requested a quote to direct customers to another part of your site.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_continue_shopping', uc_get_message('request_a_quote_continue_shopping')),
    '#rows' => 3,
  );

  if (module_exists('token')) {
    $form['request_a_quote_completion']['token_tree'] = array(
      '#markup' => theme('token_tree', array('token_types' => array('node', 'site', 'store',  'uc_product_quote',))),
    );
  }


  /*
      'request a quote' email confirmation
  */

  $form['request_a_quote_email_confirmation'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('Request a quote: email confirmation'),
  );

  $form['request_a_quote_email_confirmation']['uc_product_quote_request_a_quote_email_confirmation_template'] = array(
    '#type' => 'textfield',
    '#title' => t('Email confirmation template'),
    '#size' => 60,
    '#description' => t('Define the template file for email notification on a request for a quote creation.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_confirmation_template', 'uc-product-quote--request-a-quote-customer-confirmation'),
  );
  $form['request_a_quote_email_confirmation']['uc_product_quote_request_a_quote_email_confirmation_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include header'),
    '#description' => t('Add a header with site logo to the email.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_confirmation_header', TRUE),
  );
  $form['request_a_quote_email_confirmation']['uc_product_quote_request_a_quote_email_confirmation_details'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include details'),
    '#description' => t('Add the details of the request of the quote to the confirmation email.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_confirmation_details', TRUE),
  );
  $email_introduction_text = 'Dear,';
  $email_introduction_text .= '<p></p>';
  $email_introduction_text .= 'you have requested for a quote.';
  $email_introduction_text .= 'Our responsible will will look at your request and publish a quote for you in the next coming days.';
  $email_introduction_text .= 'We will send you a notification message when the quote is ready.';
  $form['request_a_quote_email_confirmation']['uc_product_quote_request_a_quote_email_confirmation_introduction'] = array(
    '#type' => 'textarea',
    '#title' => t('Introduction text'),
    '#description' => t('Introduction text for the e-mail that confirms the request for a quote by a customer.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_confirmation_introduction', $email_introduction_text),
    '#rows' => 6,
  );

  $email_help_text =  'Where can I find my request for a quote and the quote ? <br />';
  $email_help_text .=  'To view your request for a quote and to order the quote once published, you need to login. <br />';
  $email_help_text .=  'If you are new to [store:name], your account details and login instructions have been sent to your e-mail address. <br />';
  $email_help_text .=  'To login, click here: [site:login-link].';
  $email_help_text .=  'After login,  you can get an overview of the quotes that have been published here: [uc_product_quote:customer-published-quotes] .<br />';
  $email_help_text .=  '<p></p>';
  $email_help_text .=  'Please note: This e-mail message is an automated notification. Please do not reply to this message. <br />';
  $email_help_text .=  'Thanks again for shopping with us.<p></p>';

  $form['request_a_quote_email_confirmation']['uc_product_quote_request_a_quote_email_confirmation_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Help text'),
    '#description' => t('Help text for the e-mail that confirms the request for a quote by a customer.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_confirmation_help', $email_help_text),
    '#rows' => 15,
  );
  if (module_exists('token')) {
    $form['request_a_quote_email_confirmation']['token_tree'] = array(
      '#markup' => theme('token_tree', array('token_types' => array('site', 'store', 'uc_product_quote','uc_product'))),
    );
  }


  /*
      'request a quote' email notification
  */

  $form['request_a_quote_email_notification'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('Request a quote: email notification'),
  );

  $form['request_a_quote_email_notification']['uc_product_quote_request_a_quote_email_notification_template'] = array(
    '#type' => 'textfield',
    '#title' => t('Email notification Template'),
    '#size' => 60,
    '#description' => t('Define the template file for email notification on a request for a quote creation.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_notification_template', 'uc-product-quote--request-a-quote-shop-notification'),
  );
  $form['request_a_quote_email_notification']['uc_product_quote_request_a_quote_email_notification_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include header'),
    '#description' => t('Add a header with site logo to the email.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_notification_header', TRUE),
  );
  $form['request_a_quote_email_notification']['uc_product_quote_request_a_quote_email_notification_details'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include details'),
    '#description' => t('Add the details of the request of the quote to the notification email.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_notification_details', TRUE),
  );
  $email_introduction_text = 'Dear,';
  $email_introduction_text .= '<p></p>';
  $email_introduction_text .= 'A request for a quote has been created.';
  $email_introduction_text .= 'Please, have a look at the requested quotes and publish a quote for the customer.';
  $form['request_a_quote_email_notification']['uc_product_quote_request_a_quote_email_notification_introduction'] = array(
    '#type' => 'textarea',
    '#title' => t('Introduction text'),
    '#description' => t('Introduction text for the e-mail that notifies the shop administrator of the request for a quote by a customer.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_notification_introduction', $email_introduction_text),
    '#rows' => 6,
  );

  $email_help_text =  'Where can I find the requests for a quote ? <br />';
  $email_help_text .=  'To view the requests and publish a quote, you need to login. <br />';
  $email_help_text .=  'To login, click here: [site:login-url]. <br />';
  $email_help_text .=  'After login, you can get an overview of the quotes that have been requested here: [uc_product_quote:shopadmin-requested-quotes]  .<br />';
  $email_help_text .=  '<p></p>';
  $email_help_text .=  'Please note: This e-mail message is an automated notification. Please do not reply to this message. <br />';

  $form['request_a_quote_email_notification']['uc_product_quote_request_a_quote_email_notification_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Help text'),
    '#description' => t('Help text for the e-mail that notifies the shop administrator that a request for a quote has been created.'),
    '#default_value' => variable_get('uc_product_quote_request_a_quote_email_notification_help', $email_help_text),
    '#rows' => 15,
  );
  if (module_exists('token')) {
    $form['request_a_quote_email_notification']['token_tree'] = array(
      '#markup' => theme('token_tree', array('token_types' => array('site', 'store', 'uc_product_quote',))),
    );
  }



  /*
      'Publish a Quote' instruction settings
  */


  $form['publish_a_quote'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('Publish a quote: instruction messages'),
  );

  $instruction_header = 'To publish a quote, specify your pricing and click on the publish button; You can give additional information on your offer in the comments area.';
  $form['publish_a_quote']['uc_product_quote_edit_quote_instruction_header'] = array(
    '#type' => 'textarea',
    '#title' => t('Publish a quote: instruction header'),
    '#description' => t('Adjust the text of the instruction header on the edit quote form.'),
    '#default_value' => variable_get('uc_product_quote_edit_quote_instruction_header', $instruction_header),
  );


  $instruction_footer = "Please click on the 'Publish' button to make your quote available to the customer.";
  $instruction_footer .= '</br>';
  $instruction_footer .= "Click on the 'Save without publishing' button to keep your changes without making the offer available.";
  $instruction_footer .= "You will need to publish your offer later.";
  $instruction_footer .= '</br>';
  $instruction_footer .= "Click on the 'Cancel' button to discard all your changes.";
  $form['publish_a_quote']['uc_product_quote_edit_quote_instruction_footer'] = array(
    '#type' => 'textarea',
    '#title' => t('Publish a quote: instruction footer'),
    '#description' => t('Adjust the text of the instruction footer on the on the edit quote form.'),
    '#default_value' => variable_get('uc_product_quote_edit_quote_instruction_footer', $instruction_footer),
  );

  if (module_exists('token')) {
    $form['publish_a_quote']['token_tree'] = array(
      '#markup' => theme('token_tree', array('token_types' => array('site', 'store', 'uc_product_quote',))),
    );
  }





  /*
      'published quote' email confirmation
  */

  $form['published_quote_email_confirmation'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('Published quote: email confirmation'),
  );

  $form['published_quote_email_confirmation']['uc_product_quote_published_quote_email_confirmation_template'] = array(
    '#type' => 'textfield',
    '#title' => t('Email confirmation template'),
    '#size' => 60,
    '#description' => t('Define the template file for email confirmation when a quote is published.'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_confirmation_template', 'uc-product-quote--quotepublishedshopconfirmation'),
  );
  $form['published_quote_email_confirmation']['uc_product_quote_published_quote_email_confirmation_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include header'),
    '#description' => t('Add a header with site logo to the email.'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_confirmation_header', TRUE),
  );
  $form['published_quote_email_confirmation']['uc_product_quote_published_quote_email_confirmation_details'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include details'),
    '#description' => t('Add the details of the published quote to the confirmation email.'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_confirmation_details', TRUE),
  );
  $email_introduction_text = 'Dear,';
  $email_introduction_text .= '<p></p>';
  $email_introduction_text .= 'this is a confirmation email a quote you have published';

  $form['published_quote_email_confirmation']['uc_product_quote_published_quote_email_confirmation_introduction'] = array(
    '#type' => 'textarea',
    '#title' => t('Introduction text'),
    '#description' => t('Introduction text for the e-mail that confirms publishing of a quote by the shop administrator.'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_confirmation_introduction', $email_introduction_text),
    '#rows' => 6,
  );

  $email_help_text =  'Where can I find my published quote ? <br />';
  $email_help_text .=  'To view the published quote, you need to login. <br />';
  $email_help_text .=  'To login, click here: [site:login-url]. <br />';
  $email_help_text .=  'And you can get an overview of the quotes that have been published here: [uc_product_quote:shopadmin-published-quotes] . <br />';
  $email_help_text .=  '<p></p>';
  $email_help_text .=  'Please note: This e-mail message is an automated notification. Please do not reply to this message. <br />';


  $form['published_quote_email_confirmation']['uc_product_quote_published_quote_email_confirmation_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Help text'),
    '#description' => t('Help text for the e-mail that confirms the publishing of a quote to the shop administrator .'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_confirmation_help', $email_help_text),
    '#rows' => 15,
  );
  if (module_exists('token')) {
    $form['published_quote_email_confirmation']['token_tree'] = array(
      '#markup' => theme('token_tree', array('token_types' => array('site', 'store', 'uc_product_quote',))),
    );
  }


  /*
      'published quote' email notification
  */

  $form['published_quote_email_notification'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#group' => 'uc_product_quote_settings',
    '#title' => t('Published quote: email notification'),
  );

  $form['published_quote_email_notification']['uc_product_quote_published_quote_email_notification_template'] = array(
    '#type' => 'textfield',
    '#title' => t('Email notification Template'),
    '#size' => 60,
    '#description' => t('Define the template file for email notification on a request for a quote creation.'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_notification_template', 'uc-product-quote--quotepublishedcustomernotification'),
  );
  $form['published_quote_email_notification']['uc_product_quote_published_quote_email_notification_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include header'),
    '#description' => t('Add a header with site logo to the email.'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_notification_header', TRUE),
  );
  $form['published_quote_email_notification']['uc_product_quote_published_quote_email_notification_details'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include details'),
    '#description' => t('Add the details of the published quote to the notification email.'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_notification_details', TRUE),
  );

  $email_introduction_text = 'Dear,';
  $email_introduction_text .= '<p></p>';
  $email_introduction_text .= 'your request for a quote has been answered and a quote has been published.';
  $email_introduction_text .= 'We kindly invite you to order your quote on our website.';


  $form['published_quote_email_notification']['uc_product_quote_published_quote_email_notification_introduction'] = array(
    '#type' => 'textarea',
    '#title' => t('Introduction text'),
    '#description' => t('Introduction text for the e-mail that notifies the customer of a published quote.'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_notification_introduction', $email_introduction_text),
    '#rows' => 6,
  );



  $email_help_text =  'To view the published quote and to order the quote, you need to login. <br />';
  $email_help_text .=  'If you are new to , your account details and login instructions have been sent to your e-mail address. <br />';
  $email_help_text .=  'To login, click here:  [site:login-link]. <br />';
  $email_help_text .=  'After login, you can view and order the published quote here:  [uc_product_quote:customer-published-quotes] .<br />';
  $email_help_text .=  '<p></p>';
  $email_help_text .=  'Please note: This e-mail message is an automated notification. Please do not reply to this message. <br />';
  $email_help_text .=  'Thanks again for shopping with us.<p></p>';


  $form['published_quote_email_notification']['uc_product_quote_published_quote_email_notification_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Help text'),
    '#description' => t('Help text for the e-mail that notifies the customer that a quote has been published.'),
    '#default_value' => variable_get('uc_product_quote_published_quote_email_notification_help', $email_help_text),
    '#rows' => 15,
  );

  if (module_exists('token')) {
    $form['published_quote_email_notification']['token_tree'] = array(
      '#markup' => theme('token_tree', array('token_types' => array('site', 'store', 'uc_product_quote',))),
    );
  }




  return system_settings_form($form);
}

