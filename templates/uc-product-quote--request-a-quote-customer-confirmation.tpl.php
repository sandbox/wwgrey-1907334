<?php

/**
 * @file
 * Standard template for customer notification email for a published quote.
 *
 * following variables are available
 * $business_header TRUE or FALSE (logo, makes only sense if contents of quote are added
 * $introduction_text
 * $help_text
 * $email_text   TRUE or FALSE (add the contents of the quote in the emai body
 * $quote : quote object that has been published
 * $language_object   language object of the customer
 * $customer customer object
 * [site_current_date] => mar, 29/01/2013 - 21:56
 * [site_name]
 * [site_slogan]
 * [site_mail]
 * [site_url]
 * [site_url_brief]
 * [site_login_url]
 * [site_new_password]
 * [site_logo] =>
 * [store_name] =>
 * [store_link] =>
 * [store_owner] =>
 * [store_email] =>
 * [store_phone] =>
 * [store_fax] =>
 * [store_address] =>
 * [store_help_url] =>
 * [customer_picture] =>
 * [customer_uid] =>
 * [customer_name] =>
 * [customer_mail] =>
 * [customer_url] =>
 * [customer_edit_url] =>
 * [shop_admin_picture] =>
 * [shop_admin_uid] =>
 * [shop_admin_name] =>
 * [shop_admin_mail] =>
 * [shop_admin_url] =>
 * [shop_admin_edit_url] =>
 * [quote_customer_requested_quotes] =>
 * [quote_customer_published_quotes] =>
 * [quote_customer_quote] =>
 * [quote_shopadmin_requested_quotes] =>
 * [quote_shopadmin_published_quotes] =>
 * [quote_shopadmin_quote] =>

 */

// get the argument to translate all texts in the customers' language
$arguments = array();
$t_options = array(
  'langcode' => isset($language_object->language) ? $language_object->language : 'und',
  'context' => NULL,
);


?>




<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#FFFFFF" style="font-family: verdana, arial, helvetica; font-size: small;">





  <tr>
    <td colspan="2">
      <p><?php print $introduction_text; ?></p>
    </td>
  </tr>

  <tr>
    <td colspan="2">
      <p><?php print $help_text; ?></p>
    </td>
  </tr>
  <tr>
    <td><p> </p></td>
  </tr>
  <tr>
    <td><p> </p></td>
  </tr>



</table>

<?php if ($email_text): ?>
  <table width="95%" border="1" cellspacing="0" cellpadding="1" align="center" bgcolor="#FFFFFF" style="font-family: verdana, arial, helvetica; font-size: small;">

  <?php if ($business_header): ?>
    <tr valign="top">
      <td>
        <table width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">
          <tr>
            <td>
              <?php print $site_logo; ?>
            </td>
            <td width="98%">
              <div style="padding-left: 1em;">
                <span style="font-size: large;"><?php print " "; ?></span><br />
                <?php print ""; ?>
              </div>
            </td>
            <td nowrap="nowrap">
              <?php print ""; ?><br /><?php print ""; ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  <?php endif; ?>

    <tr valign="top">
      <td>
        <table width="100%" border="1" cellspacing="0" cellpadding="1" align="center" bgcolor="#FFFFFF" style="font-family: verdana, arial, helvetica; font-size: 16px;">
          <tr>
            <td>
              <b><?php print t('Request for a quote - details',$arguments,$t_options); ?></b>
            </td>
          </tr>
        </table>

      </td>
    </tr>

    <tr>
      <td colspan="2">

        <table width="100%" cellspacing="0" cellpadding="5" style="font-family: verdana, arial, helvetica; font-size: 12px;">
          <tr>
            <td valign="top" nowrap="nowrap" width="20%">
              <?php print t('Request Date:',$arguments,$t_options); ?><br />
            </td>
            <td valign="top" width="60%">
              <?php print format_date($quote->created,'long', "d M y, G:i", NULL, $t_options['langcode']); ?><br />
            </td>
          </tr>
          <tr>
            <td valign="top" nowrap="nowrap" width="20%">
              <?php print t('Quote number:',$arguments,$t_options); ?><br />
            </td>
            <td valign="top" width="60%">
              <b><?php print $quote->nid; ?></b><br />
            </td>
          </tr>
          <tr>
            <td valign="top" nowrap="nowrap" width="20%">
              <?php print t('Title:',$arguments,$t_options); ?><br />
            </td>
            <td valign="top" width="60%">
              <b><?php print $quote->title; ?></b><br />
            </td>
          </tr>
          <tr>
            <td><p> </p></td>
            <td><p>  </p></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="font-family: verdana, arial, helvetica; font-size: 10px;">
        <?php print $quote->body['und'][0]['value']; ?><br />
      </td>
    </tr>





  </table>
<?php endif; ?>





